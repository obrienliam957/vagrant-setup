Vagrant Setup
-------------

Sets up environment using Vagrant and VirtualBox (VB). Within VB, install guest additions `Devices -> Insert Guest 
Additions CD Images...`. This allows for proper scaling in VB.

System Pre-Requisites
---------------------

This assumes you have the following installed:

- Vagrant
- VirtualBox

It also assumes VirtualBox is using the IDE Controller as your storage controller.


Vagrant
-------

All the following commands use the Vagrantfile in the shells current directory. 

To launch a machine: `vagrant up`

To destroy a machine: `vagrant destroy -f`

To provision a machine (run through post creation tasks): `vagrant provision`


Ansible
-------

Vagrant will use ansible to provision the machine. This is done on the new machine, so isn't required on your host.

Add any of the following in your ansible.extra_vars:

| Value | Description |
|---|---|
| `gnome_install: true` | GNOME Desktop setup to load GUI rather than SSH |
| `docker_install: true` | Installs docker on machine and configures for user |

Excluding any configurations will assume they're false.